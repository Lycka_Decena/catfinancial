<?php

// AppBundle/Controller/AdminController

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Users;
use AppBundle\Entity\ClientsLawfirm;
use AppBundle\Entity\Insurance;
use AppBundle\Entity\ClientsInsurance;
use AppBundle\Entity\AgentsInsurance;

class AdminController extends Controller
{

	/**
	* @Route("/admin",name="adminPage")
	*/
	public function adminAction(Request $request)
	{

		return $this->render('admin/index.html.twig');
	}

	/**
	* This page will be index page for admin
	*
	* @Route("/admin-movies", name="adminMoviesPages")
	*/
	public function adminMoviesPages(Request $request)
	{

		$sql = 'SELECT * FROM movies WHERE movies.date BETWEEN CURDATE() AND DATE_ADD(curdate(), INTERVAL 1 YEAR)';

		$em = $this->getDoctrine()->getManager();
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();

		$movies = $stmt->fetchAll();

		return $this->render('movies/movies.html.twig',array("movies" => $movies));
	}	

	/**
	* @Route("/admin-lawfirm", name="adminLawFirmPage")
	*/
	public function adminLawFirmPage(Request $request)
	{

		$sql = 'SELECT * FROM clients_lawfirm WHERE clients_lawfirm.date BETWEEN CURDATE() AND DATE_ADD(curdate(), INTERVAL 1 WEEK)';

		//$sql = 'SELECT * FROM clients_lawfirm WHERE clients_lawfirm.date > DATE_SUB(CURDATE(), INTERVAL 1 WEEK)';

		$em = $this->getDoctrine()->getManager();
		$stmt = $em->getConnection()->prepare($sql);
		$stmt->execute();

		$appointments = $stmt->fetchAll();

		return $this->render('lawfirm/lawfirm.html.twig',array("appointments" => $appointments));
	}

	/**
	* @Route("/admin-insurance", name="adminInsurance")
	*/
	public function adminInsuranceAction(Request $request)
	{

		$agents = $this->getDoctrine()
		->getRepository('AppBundle:AgentsInsurance')
		->findAll();

		return $this->render('insurance/insurance.html.twig',array('title' => 'Agents',"agents" =>  $agents));

	}
}