<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Movies
 *
 * @ORM\Table(name="movies", uniqueConstraints={@ORM\UniqueConstraint(name="id_movie", columns={"id_movie"})})
 * @ORM\Entity
 */
class Movies
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=10, nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="venue", type="string", length=50, nullable=true)
     */
    private $venue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="smallint", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="posterName", type="string", length=250, nullable=true)
     * @Assert\Image()
     */
    private $postername;

    /**
     * @var boolean
     *
     * @ORM\Column(name="designated", type="boolean", nullable=false)
     */
    private $designated = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_movie", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMovie;



    /**
     * Set title
     *
     * @param string $title
     *
     * @return Movies
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Movies
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set venue
     *
     * @param string $venue
     *
     * @return Movies
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;

        return $this;
    }

    /**
     * Get venue
     *
     * @return string
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Movies
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Movies
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Movies
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set postername
     *
     * @param string $postername
     *
     * @return Movies
     */
    public function setPostername($postername)
    {
        $this->postername = $postername;

        return $this;
    }

    /**
     * Get postername
     *
     * @return string
     */
    public function getPostername()
    {
        return $this->postername;
    }

    /**
     * Set designated
     *
     * @param boolean $designated
     *
     * @return Movies
     */
    public function setDesignated($designated)
    {
        $this->designated = $designated;

        return $this;
    }

    /**
     * Get designated
     *
     * @return boolean
     */
    public function getDesignated()
    {
        return $this->designated;
    }

    /**
     * Get idMovie
     *
     * @return integer
     */
    public function getIdMovie()
    {
        return $this->idMovie;
    }
}
