DROP DATABASE IF EXISTS catfm;

CREATE DATABASE catfm;

USE catfm;

CREATE TABLE users(

	id_user INT(6) UNIQUE AUTO_INCREMENT PRIMARY KEY,  
	username VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(100) NOT NULL,
	admin TINYINT(1) NOT NULL DEFAULT 0

);

CREATE TABLE movies(

	id_movie INT (6) UNIQUE AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    duration VARCHAR(10),
    venue VARCHAR(50),
    date DATE,
    description VARCHAR(500),
    price SMALLINT,
    posterName VARCHAR(250),
    designated TINYINT(1) NOT NULL DEFAULT 0

);

CREATE TABLE lawfirm(

	id_lawfirm INT(1) UNIQUE AUTO_INCREMENT PRIMARY KEY,
	telephone VARCHAR(20),
    mobile VARCHAR(20) NOT NULL,
    email VARCHAR(100) NOT NULL,
    description VARCHAR(400)

);

CREATE TABLE clients_lawfirm(

	id_clientLawfirm INT(6) NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	middlename VARCHAR(100) NOT NULL,
	surname VARCHAR(100) NOT NULL,
	contact VARCHAR(20) NOT NULL,
	email VARCHAR(50),
	date DATE NOT NULL,
	time TIME NOT NULL,
	description VARCHAR(500)

);

CREATE TABLE insurance(

	id_insurance INT(6) UNIQUE AUTO_INCREMENT PRIMARY KEY,
    service CHAR(20),
    description VARCHAR(500)

);

CREATE TABLE agents_insurance(

	id_agent INT(6) NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	middlename VARCHAR(100) NOT NULL,
	surname VARCHAR(100) NOT NULL,
	dni VARCHAR(10) NOT NULL,
	contact VARCHAR(20) NOT NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(100)

);

CREATE TABLE clients_insurance(

	id_clientInsurance INT(6) NOT NULL UNIQUE AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	middlename VARCHAR(100) NOT NULL,
	surname VARCHAR(100) NOT NULL,
	birthdate DATE NOT NULL,
	contact VARCHAR(20) NOT NULL,
	passport_dni VARCHAR(10) NOT NULL,
	type_insurance ENUM('life','top health','dental','family protection') NOT NULL,
	payment ENUM('monthly','biannual','quarterly','yearly') NOT NULL,
	amount DECIMAL(5,2) NOT  NULL,
	initiation_insurance DATE NOT NULL,
	id_agent int NOT NULL,
	FOREIGN KEY (id_agent)  REFERENCES agents_insurance(id_agent) ON DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE flights(

	id_flight INT(6) UNIQUE AUTO_INCREMENT PRIMARY KEY,
    country VARCHAR(30) NOT NULL,
    airline VARCHAR(30) NOT NULL,
    startdate DATE,
    finaldate DATE,
    price DECIMAL(5,2),
    promo TINYINT(1) NOT NULL DEFAULT 0

);

CREATE TABLE clients_flights(

	id_clientFlight INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(50) NOT NULL,
	middlename VARCHAR(50) NOT NULL,
	surname VARCHAR(50) NOT NULL,
	birthdate DATE NOT NULL,
	contact VARCHAR(20) NOT NULL,
	passport VARCHAR(10) NOT NULL,
	id_flight INT(6) NOT NULL,
	startdate DATE,
    finaldate DATE,
	promo TINYINT(1) NOT NULL DEFAULT 0,
	FOREIGN KEY (id_flight) REFERENCES flights(id_flight) ON DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE events(

	id_events INT(6) UNIQUE AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    date DATE,
    time TIME,
    topic VARCHAR(100),
    place VARCHAR(40),
    poster VARCHAR(20),
    designated TINYINT(1) NOT NULL DEFAULT 0

);

CREATE TABLE contactus(

	id_contactus INT(6) UNIQUE AUTO_INCREMENT PRIMARY KEY,
    fax VARCHAR(20),
    telephone VARCHAR(13) NOT NULL,
    email VARCHAR(20) NOT NULL,
    address VARCHAR(50) NOT NULL

);