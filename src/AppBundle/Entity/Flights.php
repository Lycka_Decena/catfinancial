<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flights
 *
 * @ORM\Table(name="flights", uniqueConstraints={@ORM\UniqueConstraint(name="id_flight", columns={"id_flight"})})
 * @ORM\Entity
 */
class Flights
{
    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=30, nullable=false)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="airline", type="string", length=30, nullable=false)
     */
    private $airline;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startdate", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finaldate", type="date", nullable=true)
     */
    private $finaldate;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promo", type="boolean", nullable=false)
     */
    private $promo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_flight", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFlight;



    /**
     * Set country
     *
     * @param string $country
     *
     * @return Flights
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set airline
     *
     * @param string $airline
     *
     * @return Flights
     */
    public function setAirline($airline)
    {
        $this->airline = $airline;

        return $this;
    }

    /**
     * Get airline
     *
     * @return string
     */
    public function getAirline()
    {
        return $this->airline;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return Flights
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set finaldate
     *
     * @param \DateTime $finaldate
     *
     * @return Flights
     */
    public function setFinaldate($finaldate)
    {
        $this->finaldate = $finaldate;

        return $this;
    }

    /**
     * Get finaldate
     *
     * @return \DateTime
     */
    public function getFinaldate()
    {
        return $this->finaldate;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Flights
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set promo
     *
     * @param boolean $promo
     *
     * @return Flights
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return boolean
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Get idFlight
     *
     * @return integer
     */
    public function getIdFlight()
    {
        return $this->idFlight;
    }
}
