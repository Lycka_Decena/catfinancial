USE catfm;

/**
* Inserts for column users.
* 1 admin user and 1 user
*/

INSERT INTO users(username,password,admin)
VALUES('lycka',MD5('lycka29'),1);

INSERT INTO users(username,password,admin)
VALUES('user1',MD5('user1'),0);

/**
* Inserts for column contactus
*/

INSERT INTO contactus(fax,telephone,email,address)
VALUES('934616024','934616024','catfm@gmail.com','Carrer Trafalgar 4, 5B');

/**
* Inserts for column law firm
*/

INSERT INTO lawfirm(telephone,mobile,email,description)
VALUES ('+34 934 616 024','+34 610 536 385','catlaw@gmail.com','Le agradecemos su interés por ABOGADOS CAT LAW FIRM. Nuestro bufete ofrece servicios de asesoría y representación jurídicas en distintos ámbitos. Conozca nuestro bufete por dentro así como sus áreas de especialización.');

/**
* Inserts for column movies
*/

INSERT INTO movies(title,duration,venue,date,description,price,designated)
VALUES('The Wedding','2h 15mins','Cine Comedia','2018-05-27',
	'Lia Marquez is a spirited woman who is set to marry Philip Cordero, Cebu City’s most eligible bachelor who currently runs to clinch the mayoral position. However, things get a little bit shaky when Wado dela Costa, Lia’s ex-boyfriend, returns to win her back before he loses his chance',
	'10',1
);

INSERT INTO movies(title,duration,venue,date,description,price,designated)
VALUES('Never Not Love You','1h 45mins','Cine Comedia','2018-06-24','Starring Nadine Lustre and James Reid',
	'10',0
);


INSERT INTO movies(title,duration,venue,date,description,price,designated)
VALUES('My Perfect You','1h 45mins','Cine Comedia','2018-07-29',
	'A major heartbreak and failing career pushes the devastated Burn (Gerald Anderson) to go on a random road trip out of town. During this vacation, he meets the lively owner of The Great Escape Hostel, Abi (Pia Wurztbach). This leads to Burn\'s second chance to happiness. But what happens when Burn learns that Abi is not real? Will he choose to stay in his dreams or wake up to reality?',
	'10',0
);

INSERT INTO movies(title,duration,venue,date,description,price,designated)
VALUES('Da One That Ghost Away','2h 10mins','Cine Comedia','2018-04-29',
	'Horror-Comedy. Starring Kim Chiue and Ryan Bang',
	'10',0
);


INSERT INTO movies(title,duration,venue,date,description,price,designated)
VALUES('Cant Help Fallin\' in Love','2h 10mins','Cine Comedia','2018-03-25',
	'Romance. Starring Kathniel',
	'10',1
);


/**
* Inserts for column insurance
*
*/

INSERT INTO insurance(service,description) VALUES
('Family Protection','With this insurance your family will receive all the help it needs during this difficult time. 
We offer you one of the best funeral services on the market. DKV Family Protection offers a wide range of services and coverages.');

INSERT INTO insurance(service,description) VALUES
('Top Health', 'The best of private medical insurance, without setting any limits or boundaries against your health. 
Insurance that gives you access to the best private medicine, with whatever is needed to take care of your health and quality of life, 
with unlimited access and expenses.');

INSERT INTO insurance(service,description) VALUES
('Dental','Dental insurance that offers you a range of care and treatments, to solve your dental issues and take preventive action to help maintain good oral health.');

INSERT INTO insurance(service,description) VALUES
('Life', 'Access the best private medical care, only paying for the medical insurance you\'re interested in. Primary care, specialist doctors, 
or hospital care. Choose the module that suits your needs or combine them as you like to create your own personalised health insurance. 
The best-value option for accessing the best private healthcare.');


/**
*	Inserts for column agents_insurance
*/

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Eric","Dela Cruz","Penetrante","11223344A","666999666","eric_penetrante@gmail.com",null);

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Paul","Abiertas","Barrera","55667788B","888777111","pbarrera@gmail.com","Diagonal");

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Maria","Garcia","Lopez","22448811C","111444555","lopez.maria@gmail.com","Barcelona");

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Nancy","Smith","Collins","88997755D","777555666","collins.nancysmith@gmail.com",null);

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Adriana","Perez","Diaz","55774433F","222666999","diaz_adriana@gmail.com",null);

INSERT INTO agents_insurance(name,middlename,surname,dni,contact,email,address) VALUES
("Carlos","Rodriguez","Fernandez","66117733G","555888444","carlosrf@gmail.com","Gran Via");

/**
* Inserts for column clients_insurance
*/

INSERT INTO clients_insurance(name,middlename,surname,birthdate,contact,passport_dni,type_insurance,payment,amount,initiation_insurance,id_agent) VALUES 
("Lycka","Abiertas","Decena","1997-05-29","683423405","24392849D","family protection","yearly",58.98,"2017-10-25",2);

INSERT INTO clients_insurance(name,middlename,surname,birthdate,contact,passport_dni,type_insurance,payment,amount,initiation_insurance,id_agent) VALUES 
("Sofia","Garcia","Rosales","1993-10-29","888999777","35739865G","dental","quarterly",65.24,"2018-01-01",1);

INSERT INTO clients_insurance(name,middlename,surname,birthdate,contact,passport_dni,type_insurance,payment,amount,initiation_insurance,id_agent) VALUES 
("Angela","Alba","Martinez","1987-04-24","+34 999666999","C00002534","top health","monthly",105.50,"2016-11-05",4);

INSERT INTO clients_insurance(name,middlename,surname,birthdate,contact,passport_dni,type_insurance,payment,amount,initiation_insurance,id_agent) VALUES 
("Miguel","Lirio","Reyes","1967-11-15","+34 892435012", "759438617","life","biannual",86.21,"2017-03-01",3);

INSERT INTO clients_insurance(name,middlename,surname,birthdate,contact,passport_dni,type_insurance,payment,amount,initiation_insurance,id_agent) VALUES 
("Antonio","Velasco","Gomez","1975-08-12","668489701","AG123456","top health","quarterly",73.42,"2017-08-01",5);

/**
* Inserts for column clients_lawfirm
*/

INSERT INTO clients_lawfirm(name,middlename,surname,contact,email,date,time,description) VALUES
("Ana","Reyes","Lopez","666999888",null,"2018-05-29","09:30:00","Consult of immigration matters");

INSERT INTO clients_lawfirm(name,middlename,surname,contact,email,date,time,description) VALUES
("Jose Maria","Gomez","Rodriguez","888555222","jmgomez@gmail.com","2018-06-15","17:00:00","Work/Job laws and rights");

INSERT INTO clients_lawfirm(name,middlename,surname,contact,email,date,time,description) VALUES
("Luis","Diaz","Navarro","777444666",null,"2018-05-24","13:15:00","Consultation");

INSERT INTO clients_lawfirm(name,middlename,surname,contact,email,date,time,description) VALUES
("Paula","Hernandez","Torres","222555777",null,"2018-07-25","10:30:00","Divorce");

INSERT INTO clients_lawfirm(name,middlename,surname,contact,email,date,time,description) VALUES
("Enzo","Alba","Paredes","888777333","eaparedes@gmail.com","2018-05-29","11:00:00","Family reunification in Spain");















