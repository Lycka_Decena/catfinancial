<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientsFlights
 *
 * @ORM\Table(name="clients_flights", indexes={@ORM\Index(name="id_flight", columns={"id_flight"})})
 * @ORM\Entity
 */
class ClientsFlights
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="middlename", type="string", length=50, nullable=false)
     */
    private $middlename;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=50, nullable=false)
     */
    private $surname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=false)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=20, nullable=false)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=10, nullable=false)
     */
    private $passport;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startdate", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finaldate", type="date", nullable=true)
     */
    private $finaldate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="promo", type="boolean", nullable=false)
     */
    private $promo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clientFlight", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idClientflight;

    /**
     * @var \AppBundle\Entity\Flights
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Flights")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_flight", referencedColumnName="id_flight")
     * })
     */
    private $idFlight;



    /**
     * Set name
     *
     * @param string $name
     *
     * @return ClientsFlights
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     *
     * @return ClientsFlights
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return ClientsFlights
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return ClientsFlights
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return ClientsFlights
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return ClientsFlights
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return ClientsFlights
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set finaldate
     *
     * @param \DateTime $finaldate
     *
     * @return ClientsFlights
     */
    public function setFinaldate($finaldate)
    {
        $this->finaldate = $finaldate;

        return $this;
    }

    /**
     * Get finaldate
     *
     * @return \DateTime
     */
    public function getFinaldate()
    {
        return $this->finaldate;
    }

    /**
     * Set promo
     *
     * @param boolean $promo
     *
     * @return ClientsFlights
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return boolean
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * Get idClientflight
     *
     * @return integer
     */
    public function getIdClientflight()
    {
        return $this->idClientflight;
    }

    /**
     * Set idFlight
     *
     * @param \AppBundle\Entity\Flights $idFlight
     *
     * @return ClientsFlights
     */
    public function setIdFlight(\AppBundle\Entity\Flights $idFlight = null)
    {
        $this->idFlight = $idFlight;

        return $this;
    }

    /**
     * Get idFlight
     *
     * @return \AppBundle\Entity\Flights
     */
    public function getIdFlight()
    {
        return $this->idFlight;
    }
}
