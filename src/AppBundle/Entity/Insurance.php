<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Insurance
 *
 * @ORM\Table(name="insurance", uniqueConstraints={@ORM\UniqueConstraint(name="id_insurance", columns={"id_insurance"})})
 * @ORM\Entity
 */
class Insurance
{
    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=20, nullable=true)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_insurance", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInsurance;



    /**
     * Set service
     *
     * @param string $service
     *
     * @return Insurance
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Insurance
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get idInsurance
     *
     * @return integer
     */
    public function getIdInsurance()
    {
        return $this->idInsurance;
    }

    /**
    * To avoid having an error about not being able to convert in string
    */
    public function __toString()
    {
        //To show the name of service when selected.
        return $this->service;

        //to show the id  of the service
        //return $this->idInsurance();

    }
}
