<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//Components needed to build a form
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\Movies;

class MoviesController extends Controller
{

	/**
	* This route will be the index page for frontend
	* Which clients/viewers will see.
	*
	* @Route("/movies",name="moviespage")
	*/
	public function moviesAction(Request $request)
	{
		     $movies = $this->getDoctrine()
            ->getRepository('AppBundle:Movies')
            ->findAll();

		return $this->render('default/movies.html.twig',array("movies" => $movies));
	}

	/**
	* @Route("/listmovies", name="allmovies")
	*/
	public function listMoviesAction(Request $request)
	{

		$movies = $this->getDoctrine()->getRepository('AppBundle:Movies')->findAll();

		return $this->render('movies/movies.html.twig',array("movies" => $movies));
	}

	/**
	*
	* @Route("/addmovie", name="addmovie")
	*/
	public function addMovieAction(Request $request)
	{
		$movie = new Movies();

		$form = $this->createFormBuilder($movie)
					->add('title', TextType::class)
					->add('duration', TextType::class,array('required' =>false,))
					->add('venue', TextType::class,array('required' =>false,))
					->add('price', NumberType::class,array('required' =>false,))
					->add('postername', FileType::class, array('label' => 'Upload poster ','required' =>false))
					->add('description', TextareaType::class,array('required' =>false,))
					->add('date', DateType::class,array('widget' => 'choice',
									'years' => range(date('Y'),date('Y')+100),
									'months' => range(date('m'), 12),
									'days' => range(date('d'),31),
								))
					->add('designated', ChoiceType::class, array('choices' => [ 'Yes' => true, 'No' => false,]))
					->add('save',SubmitType::class, array('label' => 'Add Movie '))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			/**
			* @var UploadedFile file
			*/
			$file = $movie->getPostername();
			$ext = $file->guessExtension();
			$file_name = $movie->getTitle().".".$ext;

			$file->move( $this->getParameter('image_directory'),$file_name);

			$movie->setPostername($file_name);
			$em = $this->getDoctrine()->getManager();
			$em->persist($movie);
			$em->flush();

			return $this->render('movies/message.html.twig',array('title' => 'Movie Added','message' => 'Movie added: ' . $movie->getTitle()));
		}

		return $this->render('movies/form.html.twig', array('title' => 'Add Movie', 'form' => $form->createView(),
		));

	}

	/**
	*
	* @Route("/editmovie", name="editamovie")
	*/
	public function editMovieAction(Request $request)
	{

		$movie = new Movies();

		$form = $this->createFormBuilder($movie)
					->add('title', TextType::class)
					->add('save', SubmitType::class, array('label' => 'Edit Movie'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();
			$movie = $em->getRepository('AppBundle:Movies')
						->findOneByTitle($movie->getTitle());

			if(!$movie)
			{
				return $this->render("movies/message.html.twig",array('title' => 'Movie Updated','message' => 'No movie found with title of ' . $movie->getTitle()));
			}
			return $this->redirectToRoute('updateMovie',array('title' => $movie->getTitle(),));
		}

		return $this->render('movies/form.html.twig', array('title' => ' Edit Movie ', 'form' => $form->createView(),));

	}

	/**
	*
	* @Route("/updatemovie/{title}", name="updateMovie")
	*/
	public function updateMovieAction(Request $request,$title)
	{
		$em = $this->getDoctrine()->getManager();
		$movie = $em->getRepository('AppBundle:Movies')
					->findOneByTitle($title);


		$form = $this->createFormBuilder($movie)
					->add('title', TextType::class)
					->add('duration', TextType::class,array('required' =>false,))
					->add('venue', TextType::class,array('required' =>false,))
					->add('price', NumberType::class,array('required' =>false,))
					->add('postername', FileType::class, array('label' => 'Upload poster','required' =>false, 'data_class' => null))
					->add('description', TextareaType::class,array('required' =>false,))
					->add('date', DateType::class)
					->add('designated', ChoiceType::class, array('choices' => [ 'Yes' => true, 'No' => false,]))
					->add('save',SubmitType::class, array('label' => 'Update Movie '))
					->getForm();

		$form->handleRequest($request); 

		if ($form->isSubmitted() && $form->isValid())
		{
			/**
			* @var UploadedFile file
			*/
			$file = $movie->getPostername();
			$ext = $file->guessExtension();
			$file_name = $movie->getTitle().".".$ext;

			$file->move( $this->getParameter('image_directory'),$file_name);

			$movie->setPostername($file_name);

			$em->flush();
			return $this->render('movies/message.html.twig',array('title' => 'Movie Updated','message' => 'Movie ' . $movie->getTitle() . ' is updated.',));
		}

		return $this->render('movies/form.html.twig',array('title' => 'Edit Movie', 'form' => $form->createView(),));

	}

	/**
	*
	* @Route("/cancelmovie", name="cancelmovie")
	*/
	public function cancelMovieAction(Request $request)
	{
		$movie = new Movies();

		$form = $this->createFormBuilder($movie)
					->add('title', TextType::class)
					->add('save', SubmitType::class,array('label' => 'Delete Movie'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();

			$movie = $em->getRepository('AppBundle:Movies')
						->findOneByTitle($movie->getTitle());

			if(!$movie)
			{
				return $this->render('movies/message.html.twig',array('title' => 'Movie Deleted','message' => 'No movie found for the title declared ' . $movie->getTitle(),
					));

			}

			$title = $movie->getTitle();

			//Get the poster name where the image is saved
			$image = $movie->getPostername();
			$path = $this->getParameter('image_directory').'/'.$image;
			
			//to delete the actual image in the system. I used FILESYSTEM
			$fs = new Filesystem();
			$fs->remove(array($path));
			$em->remove($movie);
			$em->flush();

			return $this->render('movies/message.html.twig',array('message' => 'Movie ' . $title . ' is deleted',
																	'title' => 'Movie Deleted',));
		}

		return $this->render('movies/form.html.twig',array(
					'title' => 'Delete Movie',
					'form' => $form->createView(),
		));
	}

	/**
	*
	* @Route("/findmovie", name="findmovie")
	*/
	public function findMovieAction(Request $request)
	{

		$movie = new Movies();

		$form = $this->createFormBuilder($movie)
					->add('title', TextType::class)
					->add('save', SubmitType::class,array('label' => 'Find Movie'))
					->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{
			$movies = $this->getDoctrine()->getRepository('AppBundle:Movies')
							->findByTitle($movie->getTitle());


			if(count($movies) == 0)
			{
				return $this->render('movies/message.html.twig',array('title' => 'Search Response','message' => 'No movies found'));
			}

			return $this->render('movies/movies.html.twig',array('movies' => $movies));
		}

		return $this->render('movies/form.html.twig',array('title' => 'Find Movie', 'form' => $form->createView(),));
	}
}