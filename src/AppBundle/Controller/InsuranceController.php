<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


//Components needed to build a form
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;



//Entity needed to get datas from database
use AppBundle\Entity\Insurance;
use AppBundle\Entity\ClientsInsurance;
use AppBundle\Entity\AgentsInsurance;


class InsuranceController extends Controller
{

	/**
	* This function is to list ALL the clients that are in the database.
	*
	* @Route("/list-clients",name="allclients")
	*/
	public function listClientsAction(Request $request)
	{

		$clients = $this->getDoctrine()
		->getRepository('AppBundle:ClientsInsurance')
		->findAll();

		return $this->render('insurance/list.html.twig',array('title' => 'List of Clients',"clients" => $clients));

	}

	/**
	* @Route("/add-client", name="addclient")
	*/
	public function addClientsAction(Request $request)
	{

		$client = new ClientsInsurance();

		$form = $this->createFormBuilder($client)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('birthdate', BirthdayType::class, array('years'  => range(1920, date('Y'))))
					->add('contact', TextType::class)
					->add('passport_dni', TextType::class)
					->add('type_insurance', EntityType::class, array('class' => 'AppBundle:Insurance', 'placeholder' => 'Choose an option',
					 						'choice_label' => 'service'))
					->add('payment', ChoiceType::class,array('placeholder' => 'Choose an option', 'choices' => [
										'Monthly' => 'monthly',
										'Biannual' => 'biannual',
										'Quarterly' => 'quarterly',
										'Yearly' => 'yearly',]))
					->add('amount', NumberType::class, array('scale' => 2))
					->add('initiation_insurance', DateType::class, array('widget' => 'choice',
										'years' => range(date('Y'),date('Y')+100),
										'months' => range(date('m'), 12),
										'days' => range(date('d'),31),
										))
					->add('id_agent', EntityType::class, array('class' => 'AppBundle:AgentsInsurance', 'placeholder' => 'Choose an option',
					 						'choice_label' => 'idAgent'))
					->add('save', SubmitType::class,array('label' => 'Add Client'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();
			$em->persist($client);
			$em->flush();

			return $this->render('insurance/message.html.twig',array('title' => 'Client Added',
				'message' => 'Client added: ' . $client->getName() . ' ' . 
				$client->getSurname()));

		}

		return $this->render('insurance/form.html.twig',array('title' => 'Add Client',
			'form' => $form->createView(),));

	}

	/**
	* @Route("/edit-client", name="editclient")
	*/
	public function editClientAction(Request $request)
	{

		$client = new ClientsInsurance();

		$form = $this->createFormBuilder($client)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class, array('label' => 'Edit Client'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			$clients = $em->getRepository('AppBundle:ClientsInsurance')
							->findOneBy(array('name' => $client->getName(), 'middlename' => $client->getMiddlename(), 
								'surname' => $client->getSurname(),));
			if(!$clients)
			{
				return $this->render("insurance/message.html.twig",
					array('title' => 'Client Updated', 
						'message' => 'No client found for ' . $client->getName() . ' ' . 
						$client->getMiddlename() . ' ' . $client->getSurname()));
			}

			return $this->redirectToRoute('updateClient',array('name' => $client->getName(),
				'middlename' => $client->getMiddlename(), 'surname' => $client->getSurname(),));
		}

		return $this->render('insurance/form.html.twig',array('title' => 'Edit Client',
			 'form' => $form->createView(),));
	}


	/**
	* @Route("/update-client/{name}/{middlename}/{surname}", name="updateClient")
	*/
	public function updateClientAction(Request $request, $name, $middlename,$surname)
	{

		$em = $this->getDoctrine()->getManager();
		$client = $em->getRepository('AppBundle:ClientsInsurance')
					->findOneBy(array('name' => $name, 'middlename' => $middlename, 'surname' => $surname));

		$form = $this->createFormBuilder($client)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('birthdate', BirthdayType::class)
					->add('contact', TextType::class)
					->add('passport_dni', TextType::class)
					->add('type_insurance', EntityType::class, array('class' => 'AppBundle:Insurance', 'placeholder' => 'Choose an option',
					 						'choice_label' => 'service'))
					->add('payment', ChoiceType::class,array('placeholder' => 'Choose an option', 'choices' => [
										'Monthly' => 'monthly',
										'Biannual' => 'biannual',
										'Quarterly' => 'quarterly',
										'Yearly' => 'yearly',]))
					->add('amount', NumberType::class)
					->add('initiation_insurance', DateType::class)
					->add('id_agent', EntityType::class, array('class' => 'AppBundle:AgentsInsurance', 'placeholder' => 'Choose an option',
					 						'choice_label' => 'idAgent'))
					->add('save', SubmitType::class,array('label' => 'Add Client'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			$em->flush();

			return $this->render('insurance/message.html.twig',array('title' => 'Client Edited',
				'message' => 'Client updated: ' . $client->getName() . ' ' . 
				$client->getSurname() . ' with Passport/DNI number ' . $client->getPassportDni(),));

		}

		return $this->render('insurance/form.html.twig',array('title' => 'Add Client',
			'form' => $form->createView(),));
	}

	/**
	* @Route("/cancel-client", name="cancelClient")
	*/
	public function cancelClientAction(Request $request)
	{
		$client = new ClientsInsurance();

		$form = $this->createFormBuilder($client)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class, array('label' => 'Delete Client'))
					->getForm();

		$form->handleRequest($request);


		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();

			$client = $em->getRepository('AppBundle:ClientsInsurance')
							->findOneBy(array('name' => $client->getName(), 'middlename' => $client->getMiddlename(), 
								'surname' => $client->getSurname(),));

			if(!$client)
			{

				return $this->render('insurance/message.html.twig',array('title' => 'Client Deleted',
					'message' => 'No person found for ' . $client->getName() . ' ' . 
					$client->getMiddlename() . ' ' . $client->getSurname(),
					'title' => 'Client Deleted'));
			}

			$name = $client->getName();
			$middlename = $client->getMiddlename();
			$surname = $client->getSurname();

			$em->remove($client);
			$em->flush();

			return $this->render('insurance/message.html.twig',array('title' => 'Client Deleted',
					'message' => 'Client found ' . $name . ' ' . 
					$middlename . ' ' . $surname . ' is deleted',
					'title' => 'Client Deleted',));
		}

		return $this->render('insurance/form.html.twig',array(
						'title' => 'Delete Client',
						'form' => $form->createView(),
		));

	}


	/**
	* @Route("/find-client", name="findclient")
	*/
	public function findClientAction(Request $request)
	{
		$client = new ClientsInsurance();

		$form = $this->createFormBuilder($client)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class,array('label' => 'Find Client'))
					->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{

			$clients = $this->getDoctrine()->getRepository('AppBundle:ClientsInsurance')
								->findOneBy(array('name' => $client->getName(), 'middlename' => $client->getMiddlename(), 
								'surname' => $client->getSurname(),));

			if(count($clients) == 0)
			{
				return $this->render('insurance/message.html.twig',array('title' => 'Search Response','message' => 'No client found'));
			}

			$name = $clients->getName();
			$middlename = $clients->getMiddlename();
			$surname = $clients->getSurname();
			$birthdate = $clients->getBirthdate();
			$contact = $clients->getContact();
			$passport_dni = $clients->getPassportDni();
			$type_insurance = $clients->getTypeInsurance();
			$payment = $clients->getPayment();
			$amount = $clients->getAmount();
			$initiation_insurance = $clients->getInitiationInsurance();


			return $this->render('insurance/find.html.twig',array('title' => 'Found Searched', 'name' => $name, 'middlename' => $middlename,
				'surname' => $surname, 'birthdate' => $birthdate, 'contact' => $contact, 'passport_dni' => $passport_dni,'type_insurance' => $type_insurance,
				'payment' => $payment, 'amount' => $amount,'initiation_insurance' => $initiation_insurance));
		}

		return $this->render('insurance/form.html.twig',array('title' => 'Find Client',
			'form' => $form->createView(),
		));
	}

}
