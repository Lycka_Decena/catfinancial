<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use AppBundle\Entity\Users;
use AppBundle\Entity\Contactus;


class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

	 /**
    	  *  @Route("/login", name="login")
          */

    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils )
    {

        //message when there is an error
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
    

        return $this->render('security/login.html.twig',array( 
            'last_username' => $lastUsername, 
            'error' => $error,
        ));       
        
    }

   /**
    * @Route("/logout", name="logout")
    */
   public function logoutAction(Request $request)
   {


      return $this->render('default/index.html.twig');

   }

   /**
   * @Route("/about", name="aboutpage")
   */
   public function aboutAction(Request $request)
   {
      return $this->render('default/aboutUs.html.twig');
   }

   /**
    * @Route("/admin",name="adminpage")
    */
   public function adminAction(Request $request)
   {

        return $this->render('admin/index.html.twig');
   }

  /**
  * @Route("/contact",name="contactpage")
  */
  public function contactAction(Request $request)
  {
         $contact = $this->getDoctrine()->getRepository('AppBundle:Contactus')
            ->findAll();

            print_r($contact);

    return $this->render('default/contactUs.html.twig',array("contact" => $contact));
  }

   /**
   * @Route("/sitemap", name="sitemapPage")
   */
   public function sitemapAction(Request $request)
   {
      return $this->render('default/sitemap.html.twig');

   }

    /**
   * @Route("/disclaimer", name="avisoLegalPage")
   */
   public function avisoLegalAction(Request $request)
   {
      return $this->render('default/disclaimer.html.twig');

   }

    /**
   * @Route("/cookies", name="cookiesPage")
   */
   public function cookiesAction(Request $request)
   {
      return $this->render('default/cookies.html.twig');

   }

}
