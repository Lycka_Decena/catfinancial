<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientsInsurance
 *
 * @ORM\Table(name="clients_insurance", uniqueConstraints={@ORM\UniqueConstraint(name="id_clientInsurance", columns={"id_clientInsurance"})}, indexes={@ORM\Index(name="id_agent", columns={"id_agent"})})
 * @ORM\Entity
 */
class ClientsInsurance
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="middlename", type="string", length=100, nullable=false)
     */
    private $middlename;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=100, nullable=false)
     */
    private $surname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=false)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=20, nullable=false)
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_dni", type="string", length=10, nullable=false)
     */
    private $passportDni;

    /**
     * @var string
     *
     * @ORM\Column(name="type_insurance", type="string", nullable=false)
     */
    private $typeInsurance;

    /**
     * @var string
     *
     * @ORM\Column(name="payment", type="string", nullable=false)
     */
    private $payment;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="initiation_insurance", type="date", nullable=false)
     */
    private $initiationInsurance;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_clientInsurance", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idClientinsurance;

    /**
     * @var \AppBundle\Entity\AgentsInsurance
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AgentsInsurance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_agent", referencedColumnName="id_agent")
     * })
     */
    private $idAgent;



    /**
     * Set name
     *
     * @param string $name
     *
     * @return ClientsInsurance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     *
     * @return ClientsInsurance
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return ClientsInsurance
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     *
     * @return ClientsInsurance
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return ClientsInsurance
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set passportDni
     *
     * @param string $passportDni
     *
     * @return ClientsInsurance
     */
    public function setPassportDni($passportDni)
    {
        $this->passportDni = $passportDni;

        return $this;
    }

    /**
     * Get passportDni
     *
     * @return string
     */
    public function getPassportDni()
    {
        return $this->passportDni;
    }

    /**
     * Set typeInsurance
     *
     * @param string $typeInsurance
     *
     * @return ClientsInsurance
     */
    public function setTypeInsurance($typeInsurance)
    {
        $this->typeInsurance = $typeInsurance;

        return $this;
    }

    /**
     * Get typeInsurance
     *
     * @return string
     */
    public function getTypeInsurance()
    {
        return $this->typeInsurance;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return ClientsInsurance
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ClientsInsurance
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set initiationInsurance
     *
     * @param \DateTime $initiationInsurance
     *
     * @return ClientsInsurance
     */
    public function setInitiationInsurance($initiationInsurance)
    {
        $this->initiationInsurance = $initiationInsurance;

        return $this;
    }

    /**
     * Get initiationInsurance
     *
     * @return \DateTime
     */
    public function getInitiationInsurance()
    {
        return $this->initiationInsurance;
    }

    /**
     * Get idClientinsurance
     *
     * @return integer
     */
    public function getIdClientinsurance()
    {
        return $this->idClientinsurance;
    }

    /**
     * Set idAgent
     *
     * @param \AppBundle\Entity\AgentsInsurance $idAgent
     *
     * @return ClientsInsurance
     */
    public function setIdAgent(\AppBundle\Entity\AgentsInsurance $idAgent = null)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * Get idAgent
     *
     * @return \AppBundle\Entity\AgentsInsurance
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }
}
