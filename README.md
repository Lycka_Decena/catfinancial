# Final Project: CAT Financial

[Moqups](https://app.moqups.com/a15amidecabi@iam.cat/DUTSlV5ftr/view)

Moqups es una aplicación online donde podemos hacer frameworks para páginas web.

El enlace sirve para ver mas  o menos como quedaría la página web junto con sus funcionalidades.

[Webpage](catfinancesl.tk)

#### Ficheros 

En el directorio src/AppBundle/Controller contiene 5 ficheros:

	··1.AdminController: aqui se verán todos los servicios que tiene control el admin.

	··2.IndexController: aqui se verán todas las funciones que hace en la parte de cliente.

	··3.InsuranceController: esto es uno de los servicios que ofrece la empresa. Este fichero, contiene todas las funciones que puede haber en el dicho servicio.

	··4.LawFirmController: igual que el seguro, este servicio es uno de lo más importante. En el fichero se verán las acciones que puede hacer el administrador / usuario en este servicio.

	··5.MoviesController: Este servicio es lo más importante de todo. Este es el fichero que contiene todas sus acciones disponibles.

Aplicación desplegada en el servidor (LABS)


### Instalar la aplicación en tu servidor local

··1. Clonar el proyecto en el directorio preferido. Usando el comando, junto con el enlace de clonar, escrito abajo.

	git clone https://Lycka_Decena@bitbucket.org/Lycka_Decena/catfinancial.git

··2. Acceder a mysql para crear la base de datos y sus tablas, también para inserir los datos.

	Creación de base de datos: 

		source catfinancial/database/catfm.sql;
	
	Inserción de datos: 

		source catfinancial/database/inserts_catfm.sql;

··3. Se han de cambiar los parametros de base de datos del proyecto. Para hacerlo se tiene que modificar el fichero parameters.yml que se encuentra en el directorio catfinancial/app/config.

Muy importante cambiar el nombre de base de datos, en este caso, será catfm. También el usuario y la contraseña que tenéis definido para acceder mysql.

··4. Para ejecutarlo, tenemos que estar en la raíz del proyecto, es decir en catfinancial/

	**Ejecución:

		php bin/console server:start [tu IP]

	Aparecerá una caja verde donde te dice cómo accederlo al navegador.

**Se usa el comando php bin/console, porque se supone que estamos usando la version 3.4 de Symfony.

··5. Accede al navegador con la IP y su puerto, normalmente es, 8000
