<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


//Components needed to build a form
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;



//Entity needed to get datas from database
use AppBundle\Entity\Lawfirm;
use AppBundle\Entity\ClientsLawfirm;

class LawFirmController extends Controller
{

	/**
	* @Route("/lawfirm",name="lawfirmpage")
	*/
	public function contactAction(Request $request)
	{
		     $information = $this->getDoctrine()
            ->getRepository('AppBundle:Lawfirm')
            ->findAll();

		return $this->render('default/lawfirm.html.twig',array("information" => $information));
	}


	/**
	* This function is to list ALL the appointments that are in the database.
	*
	* @Route("/list-appointments",name="allappointments")
	*/
	public function listAppointmentsAction(Request $request)
	{

		$appointments = $this->getDoctrine()->getRepository('AppBundle:ClientsLawfirm')
						->findAll();

		return $this->render('lawfirm/lawfirm.html.twig',array("appointments" => $appointments));

	}

 
	/**
	*	
	* @Route("/add-appointment", name="addappointment")
	*/
	public function addAppointmentAction(Request $request)
	{

		$appointment = new ClientsLawfirm();

		$form = $this->createFormBuilder($appointment)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('contact', TextType::class)
					->add('email', TextType::class, array('required' => false,))
					->add('date', DateType::class, array('widget' => 'choice',
								'years' => range(date('Y'),date('Y')+100),
								'months' => range(date('m'), 12),
								'days' => range(date('d'),31),
								))
					->add('time', TimeType::class,array('widget' => 'choice',
								'hours' => range(8,21),))
					->add('description', TextareaType::class, array('required' => false,))
					->add('save', SubmitType::class,array('label' => 'Add Appointment'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();
			$em->persist($appointment);
			$em->flush();


			$date = $appointment->getDate();
			$newDate = $date->format('d-m-Y');

			return $this->render('lawfirm/message.html.twig',array('title' => 'Appointment Added',
				'message' => 'Appointment added: ' . $appointment->getName() . ' ' . 
				$appointment->getSurname() . ' = ' . $newDate,));

		}

		return $this->render('lawfirm/form.html.twig',array('title' => 'Add Appointment',
			'form' => $form->createView(),));

	}

	/**
	*
	* @Route("/edit-appointment", name="editappointment")
	*/
	public function editAppointmentAction(Request $request)
	{
		$appointment = new ClientsLawfirm();

		$form = $this->createFormBuilder($appointment)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class, array('label' => 'Edit Appointment'))
					->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			$appointments = $em->getRepository('AppBundle:ClientsLawfirm')
							->findOneBy(array('name' => $appointment->getName(),
								'middlename' => $appointment->getMiddlename(),
								'surname' => $appointment->getSurname()));

			$name = $appointment->getName();
			$middlename = $appointment->getMiddlename();
			$surname = $appointment->getSurname();

			if(!$appointments)
			{
				return $this->render("lawfirm/message.html.twig",
					array('title' => 'Appointment Message', 
						'message' => 'No appointment found for ' . $name . ' ' . 
						$middlename . ' ' . $surname));
			}

			return $this->redirectToRoute('updateAppointment', array('name' => $name,'middlename' => $middlename, 'surname' => $surname));
		}

		return $this->render('lawfirm/form.html.twig',array('title' => 'Edit Appointment',
			 'form' => $form->createView(),));
	}

	/**
	*
	* @Route("/updateAppointment/{name}/{middlename}/{surname}", name="updateAppointment")
	*/
	public function updateAppointmentAction($name, $middlename, $surname, Request $request){

		$em = $this->getDoctrine()->getManager();
		$appointment = $em->getRepository('AppBundle:ClientsLawfirm')
							->findOneBy(array('name' => $name, 'middlename' => $middlename, 'surname' => $surname));


		$form = $this->createFormBuilder($appointment)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('contact', TextType::class)
					->add('email', TextType::class,array('required' => false))
					->add('date', DateType::class)
					->add('time', TimeType::class)
					->add('description',TextareaType::class,array('required' => false))
					->add('save', SubmitType::class,array('label' => 'Update Appointment'))
					->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{
			$em->flush();

			$name = $appointment->getName();
			$surname = $appointment->getSurname();
			$date = $appointment->getDate()->format('d-m-Y');

			return $this->render('lawfirm/message.html.twig',array('title' => 'Appointment Edited',
						'message' => 'Appointment updated: ' .$name . ' ' . $surname . '  = ' .
						$date, ));
		}

		return $this->render('lawfirm/form.html.twig', array('title' => 'Add Appointment', 'form' => $form->createView(),));
	}

	/**
	* This function is to delete an appointment
	*
	* @Route("/cancel-appointment", name="cancelappointment")
	*/
	public function cancelAppointmentAction(Request $request)
	{
		$appointment = new ClientsLawfirm();

		$form = $this->createFormBuilder($appointment)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class, array('label' => 'Cancel Appointment'))
					->getForm();

		$form->handleRequest($request);


		if ($form->isSubmitted() && $form->isValid())
		{

			$em = $this->getDoctrine()->getManager();

			$appointments = $em->getRepository('AppBundle:ClientsLawfirm')
							->findOneBy(array('name' => $appointment->getName(),
								'middlename' => $appointment->getMiddlename(),
								'surname' => $appointment->getSurname(),));

			if(!$appointments)
			{

				return $this->render('lawfirm/message.html.twig',array('title' => 'Appointment Deleted',
					'message' => 'No appointment found for the person declared ' . $appointment->getName() .
					' ' . $appointment->getMiddlename() . ' ' . $appointment->getSurname(),
					'title' => 'Appointment Deleted'));
			}

			$name = $appointment->getName();
			$middlename = $appointment->getMiddlename();
			$surname = $appointment->getSurname();

			$em->remove($appointments);
			$em->flush();

			return $this->render('lawfirm/message.html.twig',array('title' => 'Appointment Deleted',
					'message' => 'Appointment for the person declared ' . $name .
					' ' . $middlename . ' ' . $surname . ' is deleted',
					'title' => 'Appointment Deleted',));
		}

		return $this->render('lawfirm/form.html.twig',array(
						'title' => 'Delete Appointment',
						'form' => $form->createView(),
		));
	}

	/**
	* @Route("/find-appointment", name="findappointment")
	*/
	public function findAppointmentAction(Request $request)
	{
		$appointment = new ClientsLawfirm();

		$form = $this->createFormBuilder($appointment)
					->add('name', TextType::class)
					->add('middlename', TextType::class)
					->add('surname', TextType::class)
					->add('save', SubmitType::class,array('label' => 'Find Appointment'))
					->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{

			$appointments = $this->getDoctrine()->getRepository('AppBundle:ClientsLawfirm')
							->findOneBy(array('name' => $appointment->getName(),
								'middlename' => $appointment->getMiddlename(),
								'surname' => $appointment->getSurname(),));


			if(count($appointments) == 0)
			{
				return $this->render('lawfirm/message.html.twig',array('title' => 'Search Response','message' => 'No appointment found'));
			}

			//return print_r($appointments);

			$name = $appointments->getName();
			$middlename = $appointments->getMiddlename();
			$surname = $appointments->getSurname();
			$email = $appointments->getEmail();
			$contact = $appointments->getContact();
			$date = $appointments->getDate();
			$time = $appointments->getTime();
			$description = $appointments->getDescription();


			return $this->render('lawfirm/find.html.twig',array('title' => 'Found Searched','name' => $name, 'middlename' => $middlename,
				'surname' => $surname, 'email' => $email,'contact' => $contact, 'date' => $date, 'time' => $time, 'description' => $description,));
		}

		return $this->render('lawfirm/form.html.twig',array('title' => 'Find an Appointment', 'form' => $form->createView(),));

	}


}