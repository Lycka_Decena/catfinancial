<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="events", uniqueConstraints={@ORM\UniqueConstraint(name="id_events", columns={"id_events"})})
 * @ORM\Entity
 */
class Events
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="time", nullable=true)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="topic", type="string", length=100, nullable=true)
     */
    private $topic;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=40, nullable=true)
     */
    private $place;

    /**
     * @var string
     *
     * @ORM\Column(name="poster", type="string", length=20, nullable=true)
     */
    private $poster;

    /**
     * @var boolean
     *
     * @ORM\Column(name="designated", type="boolean", nullable=false)
     */
    private $designated = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id_events", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEvents;



    /**
     * Set title
     *
     * @param string $title
     *
     * @return Events
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Events
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Events
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set topic
     *
     * @param string $topic
     *
     * @return Events
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Events
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set poster
     *
     * @param string $poster
     *
     * @return Events
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * Get poster
     *
     * @return string
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Set designated
     *
     * @param boolean $designated
     *
     * @return Events
     */
    public function setDesignated($designated)
    {
        $this->designated = $designated;

        return $this;
    }

    /**
     * Get designated
     *
     * @return boolean
     */
    public function getDesignated()
    {
        return $this->designated;
    }

    /**
     * Get idEvents
     *
     * @return integer
     */
    public function getIdEvents()
    {
        return $this->idEvents;
    }
}
